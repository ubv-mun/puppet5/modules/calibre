# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include calibre::install
class calibre::install {
  package { $calibre::package_name:
    ensure => $calibre::package_ensure,
  }
}
